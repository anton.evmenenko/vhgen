#include <stdio.h>
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "nlohmann/json.hpp"

void print(cv::Mat mat, int precision)
{
    std::cout << std::setprecision(precision) << std::fixed;

    for(int i=0; i<mat.size().height; i++) {
        std::cout << "[";

        for(int j=0; j<mat.size().width; j++) {
            std::cout << mat.at<double>(i,j);
            if(j != mat.size().width - 1)
                std::cout << ",\t";
            else
                std::cout << "]" << std::endl;
        }
    }
}

int main(int argc, char** argv)
{
    cv::Mat image = cv::imread("../data/ect/0_segmap.png", 1);

    if (image.empty()) {
        std::cout << "ERROR: Failed to open or find image (../data/ect/0_segmap.png)" << std::endl;
        return 0;
    }

    std::ifstream config_fs("../data/ect/config.txt");
    nlohmann::json config;
    try {
        config = nlohmann::json::parse(config_fs);
    } catch(...) {
        std::cerr << "ERROR: Failed to open / parse config file (../data/ect/config.txt)." << std::endl;
        return 0;
    }

    // std::cout << "Image size: " << image.cols << " x " << image.rows << std::endl;

    cv::Mat gray;
    cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);

    cv::Mat rgb;
    cv::cvtColor(gray, rgb, cv::COLOR_GRAY2RGB);

    std::vector<cv::Point2f> corners;
    cv::goodFeaturesToTrack(gray, corners, 4, 0.01, 10);

    if (corners.size() != 4) {
        std::cout << "ERROR: Failed to find 4 corners on the image" << std::endl;
        return 0;
    }

    cv::Size winSize = cv::Size(5, 5);
    cv::Size zeroZone = cv::Size(-1, -1);
    cv::TermCriteria criteria = cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 40, 0.001);
    cv::cornerSubPix(gray, corners, winSize, zeroZone, criteria);

    cv::Point2d center(0, 0);

    for (size_t i = 0; i < corners.size(); i++) {
        center.x += corners[i].x;
        center.y += corners[i].y;
    }

    center.x /= corners.size();
    center.y /= corners.size();

    std::sort(corners.begin(), corners.end(), [center](const cv::Point2f &a, const cv::Point2f &b){
        return std::atan2(a.y - center.y, a.x - center.x) > std::atan2(b.y - center.y, b.x - center.x);
    });

    for(size_t i = 0; i < corners.size(); i++) {
        cv::circle(rgb, corners.at(i), 20, cv::Scalar(0, 255, 0), 5);
        cv::putText(rgb, std::to_string(i + 1), cv::Point(corners[i].x, corners[i].y - 60), cv::FONT_HERSHEY_DUPLEX, 4.0, cv::Scalar(0, 255, 0), 5);
        // std::cout << " -- Corner [" << i << "]  (" << corners[i].x << "," << corners[i].y << ")" << std::endl;
    }

    cv::circle(rgb, center, 20, cv::Scalar(0, 0, 255), 5);

    cv::imwrite("corners.png", rgb);

    cv::Mat cameraMatrix = cv::Mat(3,3,cv::DataType<double>::type);

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            cameraMatrix.at<double>(i, j) = config["cam_K"][i][j];
        }
    }

    std::vector<cv::Point3d> objectPoints(4);

    for (int i = 0; i < 4; ++i) {
        objectPoints[i].x = config["corners_world_frame"][i][0];
        objectPoints[i].y = config["corners_world_frame"][i][1];
        objectPoints[i].z = config["corners_world_frame"][i][2];
    }

    cv::Mat distortionCoefficients = (cv::Mat_<double>(4, 1) << 0, 0, 0, 0);

    cv::Mat rvec;
    cv::Mat tvec;

    bool result = cv::solvePnP(objectPoints, corners, cameraMatrix, distortionCoefficients, rvec, tvec, false, cv::SOLVEPNP_ITERATIVE);

    if (!result) {
        std::cout << "ERROR: Failed to find extrinsic matrix" << std::endl;
        return 0;
    }

    cv::Mat R(3, 3, cv::DataType<double>::type);
    cv::Rodrigues(rvec, R);

    cv::Mat w2c;
    cv::hconcat(R, tvec, w2c);
    cv::Mat bottomRow = (cv::Mat_<double>(1, 4) << 0, 0, 0, 1);
    cv::vconcat(w2c, bottomRow, w2c);

    cv::Mat c2w;
    if (cv::invert(w2c, c2w) == 0) {
        std::cout << "ERROR: Failed to invert w2c matrix" << std::endl;
        return 0;
    }

    cv::Mat opencv2blender = (cv::Mat_<double>(3, 3) << 1, 0, 0, 0, -1, 0, 0, 0, -1);

    cv::Mat temp(c2w, cv::Rect(0, 0, 3, 3));
    temp = temp * opencv2blender;

    std::cout << "Success! \n\nc2w:\n" << std::endl;
    print(c2w, 2);

    return 0;
}