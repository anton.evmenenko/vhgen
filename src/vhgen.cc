#include <stdio.h>
#include <fstream>
#include <iostream>
#include <experimental/filesystem>

#include "vacancy/voxel_carver.h"

#include "nlohmann/json.hpp"

namespace fs = std::experimental::filesystem;

int main(int argc, char* argv[]) {
  if (argc == 1) {
    std::cerr << "ERROR: Please specify target name (for example, 'bunny')" << std::endl;
    return 0;
  }

  std::string data_dir{"../data/" + std::string(argv[1]) + "/"};

  std::cout << "Input folder path = " << data_dir << std::endl;

  std::ifstream config_fs(data_dir + "config.txt");
  nlohmann::json config;
  try {
    config = nlohmann::json::parse(config_fs);
  } catch(...) {
    std::cerr << "ERROR: Failed to open / parse config file (config.txt)." << std::endl;
    return 0;
  }

  std::ifstream ifs(data_dir + "0 campose.txt");
  nlohmann::json jf;

  try {
    jf = nlohmann::json::parse(ifs);
  } catch(...) {
    std::cerr << "ERROR: Failed to open / parse campose file (0 campose.txt)." << std::endl;
    return 0;
  }

  float fx = jf[0]["cam_K"][0][0];
  float fy = jf[0]["cam_K"][1][1];
  float cx = jf[0]["cam_K"][0][2];
  float cy = jf[0]["cam_K"][1][2];

  std::cout << "/************ INPUT DATA ************/" << std::endl;

  std::cout << "Camera fx = " << fx << std::endl;
  std::cout << "Camera fy = " << fy << std::endl;
  std::cout << "Camera cx = " << cx << std::endl;
  std::cout << "Camera cy = " << cy << std::endl;
  std::cout << std::endl;

  vacancy::VoxelCarver carver;
  vacancy::VoxelCarverOption option;

  option.bb_min = Eigen::Vector3f(config["bb_min"]["x"], config["bb_min"]["y"], config["bb_min"]["z"]);
  option.bb_max = Eigen::Vector3f(config["bb_max"]["x"], config["bb_max"]["y"], config["bb_max"]["z"]);

  std::cout << "Bounding box min = " << std::endl;
  std::cout << option.bb_min << std::endl;
  std::cout << std::endl;
  std::cout << "Bounding box max = " << std::endl;
  std::cout << option.bb_max << std::endl;
  std::cout << std::endl;

  option.resolution = config["resolution"];
  std::cout << "Voxel resolution = " << option.resolution << std::endl;
  std::cout << std::endl;

  carver.set_option(option);

  carver.Init();

  int width = config["image_size"]["width"];
  int height = config["image_size"]["height"];

  std::cout << "Image width = " << width << std::endl;
  std::cout << "Image height = " << height << std::endl;
  std::cout << std::endl;

  Eigen::Vector2f principal_point(cx, cy);
  Eigen::Vector2f focal_length(fx, fy);
  std::shared_ptr<vacancy::Camera> camera =
      std::make_shared<vacancy::PinholeCamera>(width, height,
                                               Eigen::Affine3d::Identity(),
                                               principal_point, focal_length);

  unsigned int poses_total = config["poses"];
  std::cout << "Number of poses = " << poses_total << std::endl;
  std::cout << std::endl;

  std::cout << "/************************************/" << std::endl;

  std::vector<Eigen::Affine3d> poses(poses_total);

  // create folder for output data
  fs::create_directory(data_dir + "generated");

  for (size_t i = 0; i < poses_total; ++i) {
    std::cout << "Pose " << std::to_string(i) << std::endl;

    std::ifstream ifs(data_dir + std::to_string(i) + " campose.txt");
    nlohmann::json jf;

    try {
      jf = nlohmann::json::parse(ifs);
    } catch(...) {
      std::cerr << "ERROR: Failed to open / parse campose file ("  + std::to_string(i) + " campose.txt)" << std::endl;
      return 0;
    }

    Eigen::Matrix4d c2w;

    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) {
        c2w(i, j) = jf[0]["cam2world_matrix"][i][j];
      }
    }

    // translate camera frame from blender to opencv
    Eigen::Matrix3d R = Eigen::Matrix3d::Zero();
    R(0, 0) = 1;
    R(1, 1) = -1;
    R(2, 2) = -1;

    c2w.block(0, 0, 3, 3) = c2w.block(0, 0, 3, 3) * R;

    poses[i] = c2w;

    camera->set_c2w(poses[i]);

    vacancy::Image1b silhouette;

    if (!silhouette.Load(data_dir + std::to_string(i) + "_segmap.png")) {
      std::cerr << "ERROR: Failed to open silhouette file ("  + std::to_string(i) + "_segmap.png)" << std::endl;
      return 0;
    }

    vacancy::Image1f sdf;
    // Carve() is the main process to update voxels. Corresponds to the fusion
    // step in KinectFusion
    carver.Carve(*camera, silhouette, &sdf);

    // save SDF visualization
    vacancy::Image3b vis_sdf;
    vacancy::SignedDistance2Color(sdf, &vis_sdf, -1.0f, 1.0f);
    // vis_sdf.WritePng(data_dir + "/sdf_" + num + ".png");
    vis_sdf.WritePng(data_dir + "generated/sdf_" + std::to_string(i) + ".png");

    vacancy::Mesh mesh;
    // voxel extraction
    // slow for algorithm itself and saving to disk
    // carver.ExtractVoxel(&mesh);
    // mesh.WritePly(data_dir + "generated/voxel_" + std::to_string(i) + ".ply");

    // marching cubes
    // smoother and faster
    carver.ExtractIsoSurface(&mesh, 0.0);
    // mesh.WritePly(data_dir + "/surface_" + num + ".ply");
    mesh.WritePly(data_dir + "generated/surface_" + std::to_string(i) + ".ply");

    std::cout << std::endl;
  }

  return 0;
}